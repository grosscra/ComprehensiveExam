\documentclass[11pt]{amsart}

\usepackage{beton} % Use Concrete font for text
\renewcommand{\bfdefault}{sbc} % Use alternate computer modern fonts for boldface
\usepackage[euler-digits,euler-hat-accent]{eulervm} % Use Euler font for math

\usepackage[margin=1.0in,footskip=0.25in,top=0.75in,left=1.00in,right=1.00in]{geometry}  % Margins

\usepackage{fancyhdr} % Header
\setlength{\headheight}{14.5pt}
\pagestyle{fancy}
\fancyfoot{}
\lhead{Presentation} % Left header
\rhead{Gross \thepage} % Right header

\usepackage{amsmath,amsfonts,amsthm,amssymb,mathtools} % Math stuff
\usepackage[italicdiff]{physics} % Derivatives
\usepackage{graphicx,caption,subcaption,tikz,pdfpages,tcolorbox,todonotes,enumitem} % Graphics/extras

% === Math shortcuts ===

% Theorems
\newtheorem{thm}{Theorem}
\newtheorem{defn}{Definition}
\newtheorem{ex}{Example}
\newtheorem{rem}{Remark}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}
\newtheorem{cor}{Corollary}

% Blackboard bold
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\E}{\mathbb{E}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\V}{\mathbb{V}}
\renewcommand{\L}{\mathbb{L}}
\renewcommand{\S}{\mathbb{S}}

% Real and complex parts
\renewcommand{\Im}{\mathrm{Im}}
\renewcommand{\Re}{\mathrm{Re}}

% Integral d
\renewcommand{\d}{\;d}

% Functions
\DeclareMathOperator{\cov}{cov}
\DeclareMathOperator{\linspan}{span}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\conv}{conv}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator*{\minimize}{minimize}

% ======================


\begin{document}

\title{Comprehensive Exam Presentation}
\author{Craig Gross\\\today}
\maketitle

\section{Introduction}%
\label{sec:introduction}

This presentation will be an overview of my notes which summarize parts of the following four papers/the surrounding literature and prove key theorems contained therein.

\nocite{gunzburger_webster_zhang_2017}
\nocite{rauhut_ward_2016}
\nocite{rauhut_schwab_2016}
\nocite{adcock_bao_brugiapaglia_2019}
\begingroup
\renewcommand{\section}[2]{}%
\bibliography{../Bibliography.bib}
\bibliographystyle{siam}
\endgroup

The main theme in the material prepared for this exam is using techniques from high-dimensional function approximation to numerically solve high-dimensional parametric PDE (e.g.\ from uncertainty quantification) using samples consisting of solutions of the PDE at various points in the parameter domain.
We begin by discussing the general problem and representations of solutions through generalized polynomial chaos expansions.
For certain classes of these problems, we then discuss regularity of solutions in the parametric domain and its translation to sparsity in the gPC representations.
We then give a more in depth discussion of using techniques from compressive sensing to reconstruct sparse solutions via an optimization problem with theoretical recovery guarantees and end with future considerations.

\section{Problem Statement}%
\label{sec:problem_statement}

We will use the standard UQ example of a stochastic stationary diffusion equation to illustrate the topics considered.

\begin{ex}
	For a spatial parameter $ x \in \Omega $ and a probability space $ (\Xi, \mathcal{F}, \P) $, we consider the stochastic PDE
	\begin{equation*}
		\begin{cases}
			-D \cdot (A(x, \omega) D u(x, \omega)) = f(x, \omega), &\text{ for } x \in \Omega, \omega \in \Xi\\
			u(x, \omega) \equiv 0, & \text{for } x \in \partial\Omega, \omega \in \Xi.
		\end{cases}
	\end{equation*}
	Note that $ u $ can be thought of as a random spatial function.
	The idea is that the diffusion coefficient $ A(x, \omega) $ and source $ f(x, \omega) $ model some random physical model.
	For example, maybe we consider diffusion in some sort of heterogeneous medium (thus, necessitating $ x $ dependence in $ A $) which contains perturbations of which we do not have full knowledge.
	If we model these imperfections in the model randomly, we find that $ A $ is then a random field over the spacial domain.

	We can recast this type of problem using a Karhunen-Lo\`eve expansion in the random fields which will reduce randomness to affine dependence on a sequence of random variables which we can take as parameters.
	Explicitly, we rewrite
	\begin{equation*}
		A(x, \omega) = \hat A_0(x) + \sum_{ n = 1}^\infty \hat A_n(x) Z_n(\omega),
	\end{equation*}
	where each $ (Z_n)_{ n = 1 }^\infty  $ is a sequence of mean-zero, unit-variance, uncorrelated random variables.
	This is possible under the very mild assumption that $ A $ is in $ L^2(\Xi) $ and the covariance function of $ A $ is continuous.

	Now we forget about the dependence on the probability space, and simply treat the $ (Z_n)_{ n = 1 }^\infty \in \Gamma $ as a (possibly-infinite, but usually high-dimensional) sequence of parameters, and rewrite the PDE (after performing a similar expansion to $ f $) as the affine parametric equation
	\begin{equation*}
		\begin{cases}
			-D \cdot (A(x, Z) D u(x, Z)) = f(x, Z), &\text{for } x \in \Omega, Z \in \Gamma \\
			u(x, Z) = 0, & \text{for } x \in \partial\Omega, Z \in \Gamma.
		\end{cases}
	\end{equation*}
\end{ex}

Though we have chosen this as our model problem, our discussion applies to a wide class of random or parameterized PDE.
We will discuss some further assumptions (e.g.\ ellipticity, inf-sup conditions, \ldots) we need on the problem later to guarantee recovery under the discussed methods.

% \section{Sampling Methods}%
% \label{sec:sampling_methods}
% 
% 
% There are many ways to approach this type of problem, but we will take the perspective of considering $ u: \Gamma \rightarrow H_0^1(\Omega) $ to be a high dimensional function over the parameter domain in some Bochner space which we wish to approximate based on its values at points in $ \Gamma $.
% 
% A good starting point is the idea of Monte-Carlo methods.
% Returning to the idea that the parameter sequence is a sequence of random variables, we view $ u(Z(\omega)) \in H_0^1(\Omega) $ as a Banach-space valued random element.
% Randomly sampling a set of i.i.d.\ parameter values $ \left( Z^{ (m) } \right)_{ m = 1  }^M $ from the distribution of $ \Gamma $, we solve the deterministic problem at $ Z^{ (k) } $ (usually with a finite element discretization) and assemble the solution ensemble $ u_m = u(Z^{ (m)}) $.
% Then taking the empirical average of these solutions, we obtain an approximation to the expected solution, that is,
% \begin{equation*}
% 	\frac{1}{M} \sum_{ m = 1 }^M u_m \rightarrow \E u(Z)
% \end{equation*}
% as $ M \rightarrow \infty $.
% 
% One very nice property of the Monte-Carlo technique is that no matter how long a parameter sequence we consider, the empirical averages approach the expectation in the Bochner space $ L^2_\pi(\Gamma; H_0^1(\Omega)) $ (where $ \pi $ is the distribution of $ Z $) at a dimension independent rate of $ O(m^{ -1/2 }) $.
% However, this convergence is rather slow and even though in uncertainty quantification problems, we generally wish to calculate functionals of our solution (quantities of interest), Monte-Carlo solutions only allow for us to approximate statistics of the solution such as expectations and second moments.
% However, we still obtain full fidelity of the solution in the spatial direction, and can approximate any spatial QoI.
% 
% \section{Interpolation}%
% \label{sec:interpolation}
% 
% Instead of randomly choosing parameter values, we can make specific choices, and use these choices to reassemble a solution.
% Under the lens of polynomial interpolation, for some given set of one-dimensional sampling parameter values $ \{Z_n^{ (k) }\}_{ k = 1 }^{ m(n) } $ we define the associated univariate interpolation operator
% \begin{equation*}
% 	\mathcal{U}_n u = \sum_{ k = 1 }^{ m(n) } u(Z_n^{ (k) }) \phi_n^{ (k) }(Z),
% \end{equation*}
% where the $ \phi^{ (k) }_n $ are the Lagrange interpolating polynomials satisfying $ \phi_n^{ (k) }(Z_n^{ (j) }) = \delta_{ kj } $.
% A tensor product multivariate interpolation can then be defined with a sequence of one-dimensional interpolation points in each dimension as
% \begin{equation*}
% 	\mathcal{I} u = \bigotimes_{ n = 1 }^N \mathcal{U}_n u = \sum_{ \nu \in \prod_{ n = 1 }^N [m(n)] } u\left(Z_1^{ (\nu_1) }, \ldots, Z_N^{ (\nu_N) }\right) \phi_1^{ (\nu_1) }(Z_1) \cdots \phi_N^{ (\nu_N) }(Z_N).
% \end{equation*}
% 
% \section{Sparse Grids}%
% \label{sec:sparse_grids}
% 
% If we have some hierarchy of collocation points $ \{Z_{ n, l_n }^{ (k) }\}_{ k = 1 }^{ m(l_n) } $ where the $ l $ parameter defines the level of this specific collocation set in the hierarchy, we can produce better and better approximations of $ u $ by using higher and higher levels of collocation points in each direction of the tensor product interpolation.
% However, our tensor product grid of interpolation points will be exponential in the length of our parameter sequence, which we assumed was large or even countably infinite.
% Thus, when reconstruction bounds are posed in terms of the number of points used in the reconstruction, the convergence will deteriorate exponentially based on the dimension of $ \Gamma $, e.g.\
% \begin{equation*}
% 	\norm{ u - u_{ M } } \leq M^{ -r / N }.
% \end{equation*}
% 
% To avoid this ``curse of dimensionality'', we use sparse grids.
% The very general idea of sparse grids is that instead of using the full tensor product grid at the highest level, we make smart choices of combining tensor product interpolations of $ u $ at \emph{various lower levels}.
% This multilevel approach uses fewer points (especially when the collocation points from level to level are nested) while still maintaining suitable reconstruction guarantees.
% (A drawing of the index set combination might be useful here).
% 
% Though the general sparse grid construction technique is rather strictly formulated, there are some choices to make.
% The one which is better able to avoid dependence on the dimension of the parameter space is to adapt the level used in each dimension to the function being approximated.
% Based on the affine nature of the differential operator, we are able to show that the solution must be analytic in the parameter domain with the region of analyticity in dimension $ n $ depending directly on $ \hat A_n $.
% By choosing the sparse grid to use more points in dimensions where the solution is less analytic (or where these coefficients act ``poorly'' in some respect), we can remove all dependence of the dimension in the reconstruction bounds.
% Thus, we calculate the rate of convergence only depending on the original PDE and avoid the curse of dimensionality through dependence on $ N $.

\section{GPC Expansions}%
\label{sec:series_expansions}

Motivated by classical spectral collocation techniques, we represent our solution as a generalized Fourier series in the parameter domain.
For this, we need an orthonormal basis of $ L^2_\pi(\Gamma) $ which we denote $ (\phi_\nu)_{ \nu \in \Lambda} $, and consider the generalized Fourier series
\begin{equation*}
	u(Z) = \sum_{ \nu \in \Lambda } \hat u_\nu \phi_\nu(Z), \quad \hat u_\nu = \int_{ \Gamma } u(Z) \phi(Z) \d Z \in H_0^1(\Omega).
\end{equation*}
In order to have these quantities make sense, we generally either truncate the parameter sequence so that $ \Lambda = \N_0^{ N } $, or choose $ \Lambda = \{\nu \in \N_0^{ \N_+ } \mid |\supp(\nu)| < \infty\} $.
Note that instead of calculating $ \hat u_\nu $ via the integral formula (for which we need to know $ u $ anyways), we will be computing the coefficients approximately and using these series as approximations of the original solution.

Common choices for $ \{ \phi_\nu \}_{ \nu \in \Lambda } $ are algebraic polynomials, in which case we call this generalized Fourier series a generalized polynomial chaos (gPC) expansion.
For high dimensional problems such as these, it additionally makes sense to choose these algebraic polynomials as tensor products of a set of polynomial bases in each dimension, that is, for a univariate gPC basis $ \{\psi_j\}_{ j = 1 }^\infty $, choose
\begin{equation*}
	\phi_{ \nu } = \prod_{ n = 1 }^\infty \psi_{ \nu_n }.
\end{equation*}

\section{Parametric Regularity}%
\label{sec:parametric_regularity}

It turns out that under some simple summability conditions for the class of affine parametric operators in the form discussed previously, solutions to the corresponding PDE are analytic in the parameter domain.
With this analyticity in mind, we present a result on the resulting exponential decay of gPC coefficients.
For concreteness, as in \cite{rauhut_schwab_2016}, we restrict our attention in this section to Chebyshev expansions.

\begin{thm}
	Consider a neighborhood of the complexification of $ \Gamma $ as some polydisc
	\begin{equation*}
		\Gamma \subseteq \mathcal{D} = \prod_{ n \geq 1 } \mathcal{D}_{ \rho_n },
	\end{equation*}
	where $ \mathcal{D}_{ \rho_n } := \left\{ z_n \in \C \mid |z| \leq \rho_n \right\} $ with $ \rho_n > 1 $.
	If the restriction of complexification of some function $ u:\Gamma \rightarrow \C $ in each dimension is analytic and bounded on $ \mathcal{D} $, that is, $ u(z_n, z_n^*) $ is analytic in $ \mathcal{D}_{ \rho_n } $ for all $ z_n^* \in \prod_{ j \neq n } \mathcal{D}_{ \rho_j } $, then the Chebyshev coefficients satisfy exponential decay of the form
	\begin{equation*}
		\abs{ \hat u_{ \nu } }  \leq \norm{ u }_{ L^\infty( \mathcal{D} ) } 2^{ \norm{ \nu }_0 / 2} \rho^{ -\nu }.
	\end{equation*}
\end{thm}
\begin{proof}
	The result comes from an application of Cauchy's theorem to the integral definition of the Chebyshev coefficient $ \hat u_\nu $.
	Letting $ \phi_\nu $ be a tensorized Chebyshev polynomial, the collection of which form an orthonormal basis for $ L_\pi^2(\Gamma) $ with tensorized Chebyshev density
	\begin{equation*}
		\pi = \prod_{ n \geq 1 } \pi_n(z_n) := \prod_{ n \geq 1 } \frac{1}{\pi \sqrt{ 1 - z_n^2 }} ,
	\end{equation*}
	we can first calculate
	\begin{equation*}
		\abs{ \hat u_0 } = \abs{ \int_{ \Gamma } u(z) \pi(z) \d z } \leq \norm{ u }_{ L^\infty( \mathcal{D} ) }
	\end{equation*}
	since $ \phi_0 \equiv 1 $.

	For fixed $ z_n^* $, we now bound an integral of the form
	\begin{equation*}
		I_n = \int_{ -1 }^1 u(z_n, z_n^*) \phi_{ \nu_n }(z_n) \pi_n(z_n) \d z_n,
	\end{equation*}
	which, after iterating over all $ n \in \supp(\nu) $ for some multiindex $ \nu $ will give the result.
	Since $ \phi_{ \nu_n }(z_n) = \sqrt{ 2 } \cos( \nu_n \arccos(z_n)) $, a change of variables $ z_n = \cos(\theta) $ results in
	\begin{equation*}
		I_n = \frac{\sqrt{ 2 }}{2\pi} \int_{ -\pi }^\pi u(\cos(\theta), z_n^*) \cos(\nu_n \theta) \d \theta = \frac{\sqrt{ 2 }}{4i\pi} \int_{ |\zeta| = 1 } u \left( \frac{\zeta + \zeta^{ -1 }}{2}, z_n^* \right) \left( \zeta^{ \nu_n } + \zeta^{ -\nu_n } \right) \frac{\d \zeta}{\zeta},
	\end{equation*}
	where we have switched to integrating in the complex plane by the further change of variables $ \zeta = e^{ i\theta } $.
	
	The goal now is to use the trailing powers of $ \zeta $ to produce some sort of exponential decay in $ \nu_n $.
	For example, if we split the integral in two and suppress the function evaluation of $ u $, we have the second piece of $ |I_n| $ bounded by
	\begin{equation*}
		\frac{\sqrt{ 2 }}{4\pi} \int_{ |\zeta| = 1 } |u| |\zeta|^{ -\nu_n - 1 } \d \zeta.
	\end{equation*}
	If we instead integrate over a circle $ |\zeta| = r $ with $ r $ as large as possible, we can produce the desired decay.
	By Cauchy's theorem, we just need to deform $ |\zeta| = 1 $ to $ |\zeta| = r $ through a region where $ u \left( \frac{\zeta + \zeta^{ -1 }}{2}, z_n^* \right) $ is analytic.
	Rewriting the regions where $ u $ is evaluated in polar form, we find for $ \sigma > 1 $
	\begin{equation*}
		\left\{ \frac{\zeta + \zeta^{ -1 }}{2} \mid |\zeta| = \sigma \right\} = \left\{ \frac{\sigma + \sigma^{ -1 }}{2} \cos(\theta) - i \frac{\sigma - \sigma^{ -1 }}{2} \sin(\theta) \mid \theta \in [0, 2\pi)  \right\}
	\end{equation*}
	that these are the so called ``Bernstein ellipses'' with semiaxes bounded by $ \sigma $.
	(Draw a picture of the values of $ (\zeta + \zeta^{ -1 }) / 2 $ moving around the circle $ |\zeta| = \sigma $ and show that these fit into the disc of radius $ \sigma $).
	Thus, these ellipses will at least fit into discs with radii $ \sigma $, and so we can take $ \sigma \leq r$ maximally to be $ \rho_n $ where $ u(z_n, z_n^*) $ is analytic.
	Deforming the piece of $ |I_n| $ considered above, 
	\begin{equation*}
		\frac{\sqrt{ 2 }}{4i\pi} \int_{ |\zeta| = 1 } |u| |\zeta|^{ -\nu_n - 1 } \d \zeta = \frac{\sqrt{ 2 }}{4i \pi } \int_{ |\zeta| = \rho_n } |u| |\zeta|^{ -\nu_n - 1 } \d \zeta = \frac{1}{\sqrt{ 2 }} \norm{ u }_{ L^\infty( \mathcal{D}) }\rho_n^{ -\nu_n }.
	\end{equation*}
	The same analysis applies to the first piece, where the same Bernstein ellipses are simply traversed backwards as we move from $ \sigma = 1 $ to $ \sigma = \rho_n^{ -1 } $, where, after iterating as discussed earlier, proves the desired convergence of Chebyshev coefficients.
\end{proof}

This key theorem becomes the basis for showing that solutions to the considered affine operator have Chebyshev coefficients living in ``weighted $ \ell_p $'' spaces, that is, the space of all multiindexed sequences with finite weighted $ \ell_p $ norms
\begin{equation*}
	\norm{ \hat u }_{ \omega, p } = \left( \sum_{ \nu \in \Lambda } \omega_\nu^{ 2 - p } |\hat u|^p \right)^{ 1 / p },
\end{equation*}
for a multiindexed sequence of weights $ \omega_\nu \geq 1 $ and $ p < 1 $.
In general, these weights take a form very similar to $ 2^{ \norm{ \nu }_0 / 2 } \rho^{ -\nu } $ as above, but take the opportunity of the decay in the coefficients to grow exponentially in $ \nu $.

We can also consider this implication of weighted summability from analyticity in a complementary perspective.
Based on the fast decay of coefficients as $ \nu $ increases, we should be able to obtain sufficiently accurate approximations to the solution through truncations of the Chebyshev series.
This brings us to consider \emph{sparse} Chebyshev approximations to the function, and in fact, we will consider an extension to weighted sparse Chebyshev approximations.
We define the error in the best weighted $ s $-term approximation to the coefficients of $ u $ by
\begin{equation*}
	\sigma_{ s }(\hat u)_{ \omega, p } = \inf_{ \sum_{ \nu \in \supp(z) } \omega_\nu^2 \leq s } \norm{ \hat u - z }_{ \omega, p }.
\end{equation*}
We thus consider a function sufficiently compressible if this quantity is small for small $ s $.
Returning to the weighted summability of $ u $, we can quantify this compressibility as $ s $ grows by the weighted Stechkin estimate, which states that
\begin{equation*}
	\sigma_{ s }(\hat u)_{ \omega, q } \leq (s - \norm{ \omega }_\infty^2 )^{ 1/q - 1/p } \norm{ \hat u }_{ \omega, p }.
\end{equation*}
Thus, by showing weighted $ \ell_p $ compressibility with smaller values of $ p $, we rewarded with a steep drop off in the error obtained by truncating the coefficient sequence by successively more terms.

\section{Compressive Sensing}%
\label{sec:compressive_sensing}

We now build on these ideas of sparse approximations and compressibility to attempt to find an approximate solution.
By sampling values of the PDE at values in the parameter domain, we can recast this problem as attempting to reconstruct the approximately sparse vector of coefficients from a collection of linear measurements, a problem well studied in the field of compressive sensing.
To be explicit, by writing our sampling points in the parameter domain as $ Z^{ (k) } $, we define the measurements 
\begin{equation*}
	y_k = u(Z^{ (k) }) = \sum_{ \nu \in \Lambda } \hat u_\nu \phi_\nu(Z^{ (k) }),
\end{equation*}
where the value of the gPC basis at $ Z^{ (k) } $ is known.
If for now we assume that the sequence of coefficients is finite, we assemble the $ K $ measurements of the $ M = |\Lambda| $ coefficients into matrix form, giving the $ K \times M $ system
\begin{equation*}
	A \hat u = y, \qquad A_{ k,\nu } = \phi_\nu(Z^{ (k) }).
\end{equation*}
Since numerical solutions of the PDE are in general expensive, and so we wish to calculate as few of these samples as possible.
Thus, the system is assumed to be undetermined.

% From our discussion of sparse grid methods, it turns out the solution is analytic in the parameter domain, which generally means fast decay of the gPC coefficients.
% In this case, we can make the assumption that the coefficient sequence is sparse, in which case reconstructing the coefficient sequence becomes a compressive sensing problem.
% Additionally, when the measurements are randomly sampled in the same way as in Monte-Carlo sampling, from the law of $ Z $ (or whatever measure the gPC basis is orthonormal with respect to) for a sufficiently large number of samples, the sampling matrix $ A $ has been shown to satisfy the \emph{restricted isometry property} with high probability, that is, for all $ s $-sparse vectors $ x $,
When the parameter values for each measurement are chosen at random, sampled from the orthogonalization measure of the gPC basis, a sufficiently large number of samples guarantees that the sampling matrix $ A $ has the \emph{restricted isometry property} with high probability, that is, for all $ s $-sparse vectors $ x $,
\begin{equation*}
	\abs{ \norm{ Ax }_2 - \norm{ x }_2 } \leq \delta_s \norm{ x }_2,
\end{equation*}
for some $ \delta_s $ small enough.
This condition for $ \delta_{ 3s } < 1/3 $ implies a robust null-space property for $ A $, that is, for all $ S \subseteq \Lambda $ with $ |S| \leq s $, 
\begin{equation*}
	\norm{ x_S }_2 \leq \frac{\rho}{\sqrt{ s }}  \norm{ x_{ S^C } }_1 + \tau \norm{ Ax }_2,
\end{equation*}
for parameters $ \rho \in (0, 1) $ and $ \tau > 0 $.
It is this null-space property which implies stable recovery of vectors based on a minimization program.
For $ x^\sharp $ the minimizer of
\begin{equation*}
	\minimize_{ z \in \C^M } \norm{ z }_{ \omega, 1 } \quad \text{subject to } \norm{ Az - y }_2 \leq \eta,
\end{equation*}
we have the reconstruction guarantees when applied to the vector of coefficients that
\begin{gather*}
	\norm{ u - u^\sharp }_2 \leq \frac{C_1}{\sqrt{ s }}  \sigma_s(u)_1 + C_2 \frac{\eta}{\sqrt{K}},
\end{gather*}
where $ \sigma_s(u)_1 = \inf_{ \|z\|_0 \leq s } \norm{ \hat u - z }_1 $ is the error in the best $ s $-term estimate of the gPC coefficients of $ u $.

However, under this standard sparsity assumption, we find ourselves in some dimensional trouble.
The number of samples necessary for the sampling matrix to satisfy these nice recovery bounds is
\[
	K \gtrsim W^2 s \log^4(M),
\]
where $ W = \sup_{ \nu \in \Lambda } \norm{ \phi_\nu }_\infty $.
In general, the tensorized gPC polynomials have $ L^\infty $ norms which scale exponentially in the dimension of $ \Gamma $, e.g., as we have seen, $ L^2 $-normalized Chebyshev polynomials have $ \norm{ T_j }_\infty  = \sqrt{ 2 } $ for all $ j > 1 $, and so, $ W = 2^{ N/2 } $ for $ \Lambda \subseteq \N_0^N $.
Thus, the measurements again must scale exponentially in the number of dimensions which is computationally intractable for even moderately large dimensions. 

\section{Weighted Compressive Sensing}%
\label{sec:weighted_compressive_sensing}

But we now turn to the reason that we discussed weighted summability and sparsity for our analytic solution in the first place.
A successful strategy in the field of high-dimensional function approximation for alleviating the curse of dimensionality is to use anisotropic behavior in the smoothness of the function being approximated, introducing this information into the approximation methods.
The aforementioned weighting scheme does just that and is the solution for avoiding exponentially growing numbers of measurements.
By recasting all of the previously discussed quantities in terms of weighted $ \ell_p $ norms, with the additional assumption that $ \omega_n \geq \norm{ \phi_\nu }_\infty $, we remove any dependence on the $ L^\infty $-norm of the gPC basis functions in $ K $, with
\begin{equation*}
	K \gtrsim s \log^3(s) \log(M)
\end{equation*}
sufficing to ensure that the sampling matrix has a weighted restricted isometry property of order $ s $.
The resulting recovery bounds on the function are then
\begin{gather*}
	\norm{ u - u^\sharp }_{ \infty } \leq B_1 \sigma_s(u)_{ \omega, 1 } + B_2 \sqrt{ s } \frac{\eta}{\sqrt{K}},\\
	\norm{ u - u^\sharp }_2 \leq \frac{C_1}{\sqrt{ s }}  \sigma_s(u)_{ \omega, 1 } + C_2 \frac{\eta}{\sqrt{K}}.
\end{gather*}

The overall strategy of exploiting anisotropy in the solution in this case can be seen as breaking down the necessary bound on $ \norm{ \phi_\nu }_\infty $ as finely as possible in the form of weights, which can be transferred into more stringent norms and sparsity requirements for the function being approximated.
Therefore recovery results are reworked in terms of $ \sigma_s(x)_{ \omega, 1 } $ which is the exact quantity for which the Stechkin estimate and weighted summability of the solutions to affine operator equations provide decay estimates.

The proof of the (RIP) $ \implies $ (NSP) $ \implies$ (Robust Sparse Recovery) in the weighted setup does not change much from the traditional techniques.
The most involved implication is showing that the sampling matrix has the weighted RIP.
The general outline involves rewriting the $ \omega $-RIP constant as the supremum of a stochastic process over an index set of weighted sparse vectors, which we then bound using Dudley's inequality which involves integrating functions of covering numbers of this index set.
The bounds for the covering numbers come from volumetric arguments relating to packing numbers as well as Maurey's lemma, which uses the probabilistic method to show that the convex hull of a set of points has nicely controlled nets.
After showing that the expected value of the $ \omega $-RIP constant is bounded, we use Bernstein's inequality for empirical processes to show that the bound holds with high probability.

% \section{Linking PDE Solutions to Compressive Sensing}%
% \label{sec:linking_pde_solutions_to_compressive_sensing}
% 
% 
% As discussed for sparse grids, the analyticity in the parameter domain is a direct consequence of the structure of the affine parametric operator.
% In order to use the weighted compressed sensing techniques to show convergence to the true solution, we must apply a similar argument to show that this analyticity can ensure that the weighted best $ s $-term approximation is small.
% Using Stechkin's estimate, which is a result on the compressibility stating that $ \sigma_s(u)_{ \omega, 1 } \leq C_p s^{ 1 - 1/p } \norm{ u }_{ \omega, p } $ when $ \hat u \in \ell_{ \omega, p } $,
% so long as we can show that the gPC coefficients of the solution are in $ \ell_{ \omega, p } $, for some $ p < 1 $ we can provide control over how our approximation will converge in terms of $ s $ which is intrinsically linked to the number of measurements and the size of the index set by $ K \asymp s \log(M) \log^3(s) $ which is a requirement for the sensing matrix to have the weighted restricted isometry property.
% 
% For the purposes of showing $ \hat u \in \ell_{ \omega, p } $, it is simpler to restrict to only the case where the gPC basis $ \phi_\nu $ is Chebyshev polynomials (which also happen to have nice bounds on their $ L^\infty $ norms).
% Directly integrating the Chebyshev coefficients allows us to prove exponential decay with rates depending on the summability of the norms of the components of the affine operators, which can then be exploited to show $ \ell_{\omega, p} $ summability for weights $ \omega $ again depending on the norms of the affine operators.
% Once we have this, we can construct the measurements which represent (discrete) PDE solves for (random) fixed parameter values.
% Assuming that the errors in this discretization are bounded by those in the compressive sensing recovery, we then show that the solution from sparse recovery is guaranteed to converge to the exact solution.
% 
% 
% \section{Sources of Errors}%
% \label{sec:sources_of_errors}
% 
% In the compressive sensing algorithm, we recall that we need to solve the weighted $ \ell_1 $ minimization program
% \begin{equation*}
% 	\minimize_{ z \in \C^M } \norm{ z }_{ \omega, 1 } \quad \text{subject to } \norm{ Az - y } \leq \eta,
% \end{equation*}
% where $ \eta $ is an $ \ell_2 $ bound on the error between the measurements $ y $ and the true values $ u(Z^{ (k) }) $.
% As we just discussed, measurement errors for these gPC problems are unavoidable, as we must always truncate the infinite index set $ \Lambda = \N_0^N $ to a finite index set $ \Lambda_0 $ as well as the error in the discretization and solving of the PDE, usually done through a finite element method.
% Though these types of errors also occur in the sparse grid solution techniques, it is not necessary to have a bound on them before running the reconstruction algorithm.
% 
% It is also possible to consider corruption in the measurements.
% Since one of the major benefits of sampling methods is their parallelizability, we can imagine the sampling process taking place in some distributed system.
% If there are issues with the distributed setup (for example, a node failure), it is possible that a (small) subset of the returned measurements are innaccurate.
% Unlike the previous errors which are assumed to be small but pervasive, in each measurement, this corruption error may be arbitrarily large, but sparsely distributed through the measurements.
% 
% Thus, we find ourselves using using noisy measurements of the true function to recover the truncation.
% In order to even run this minimization problem, we then need a bound on the error incurred by the truncation and discretization.
% In practice this error term has been ignored, and the equality constraint used instead.
% 
% 
% \section{Alternatives to Constrained Basis Pursuit}%
% \label{sec:alternatives_to_constrained_basis_pursuit}
% 
% \todo[inline]{Note: The following sections are still sketchy since I haven't finished writing notes on these papers.
% They will be more fleshed out as I finish my lectures.
% Any concepts which I hope to expand on will be noted with to-dos.}
% 
% As a way to avoid a priori knowledge of the error, we can replace the constrained weighted $ \ell_1 $ minimization with other routines not requiring a constraint, such as
% \begin{description}
% 	\item[Weighted LASSO]
% 		\begin{equation*}
% 			x^\sharp = \minimize_{ z \in \C^M } \norm{ z }_{ \omega, 1 } + \lambda \norm{ Az - y }_2^2,
% 		\end{equation*}
% 	\item[Weighted Square-Root LASSO]
% 		\begin{equation*}
% 			x^\sharp = \minimize_{ z \in \C^M } \norm{ z }_{ \omega, 1 } + \lambda \norm{ Az - y }_2,
% 		\end{equation*}
% 	\item[Weighted LAD-LASSO]
% 		\begin{equation*}
% 			x^\sharp = \minimize_{ z \in \C^M } \norm{ z }_{ \omega, 1 } + \lambda \norm{ Az - y }_1.
% 		\end{equation*}
% \end{description}
% 
% These methods are have theoretical convergence guarantees and are shown to do well numerically, specifically weighted square-root LASSO for the standard error case and weighted LAD-LASSO for the corruption error.
% \todo[inline]{What is necessary for convergence?}
% \todo[inline]{Discussion of revision of weighted sparsity to lower sparsity?}

\section{Index Set Truncation}
\label{sec:index_set_truncation}

 A final important consideration to make is the fact that the results for recovery via weighted $ \ell_1  $ minimization are only defined over finite index sets $ \Lambda $.
 However, even though the solution does have exponentially decaying coefficients, the infinite (and sometimes even infinite dimensional) index set does have to be truncated in practice.
 The way to get around this is to choose some sufficient truncation and run the recovery on the truncated function.
 Though the measurements are (perturbations) of the exact solution, we can view them as perturbations of the truncated solution, introducing this as error in the recovery results.
 The truncation must then be chosen to balance this noise in terms of $ \sigma_s(u)_{ \omega, 1 } $ (which is in terms of $ s^{ 1 - 1/p } \norm{ u }_{ \omega, p } $).
 The assumption that on the truncation, $ \omega_\nu^2 \leq s / 2 $ is enough (and is in fact necessary for the previous sparse recovery), and is able to show that with high probability, this error can in fact be balanced.

The compressed sensing methods have been shown to have avoided the curse of dimensionality in the number of PDE solves required when coefficient weighting is employed.
However, this can side step a second possible ``computational curse of dimensionality'' hiding in the $ \ell_1 $-minimization procedure.
Due to the established anisotropy in the parametric dimensions, the considered truncation has size that usually grows subexponentially in the parameter dimension.
In certain situations where a large number of the operators $ \hat A_n(x) Z_n $ make strong contributions to the affine operator, this anisotropy becomes weaker, forcing the size of the truncation $ M $ to depend closer to exponentially on $ N $.
Since the number of measurements is logarithmically dependent on $ M $, this still avoids a curse of dimensionality in the number of PDE solves.
However, the $ \ell_1 $-minimization procedure will generally involve enumerating the basis functions on the truncation, and when $ M $ is exponential in $ N $ we find these minimization programs intractable.

Additionally, alternative truncation methods have been considered where instead of depending on weights, a general lower set sparsity in the coefficient sequence is assumed encouraging a truncation to the hyperbolic cross index set 
\begin{equation*}
	\Lambda_{ \mathrm{HC} }(s) = \left\{ \nu \in \N^N_0 \mid \prod_{ n = 1 }^N (\nu_n + 1) \leq s \right\}
\end{equation*}
which is the union of all lower sets of a given cardinality.
Even in these cases, bounds for the size of the hyperbolic cross scale as
\begin{equation*}
	M \leq \min \left\{ 2 s^3 4^N, e^2 s^{ 2 + \lg(N) } \right\}.
\end{equation*}
For large values of $ N $, this basis size will still be too large for weighted $ \ell_1 $ minimization algorithms.

\section{Related Work and Future Research Topics}%
\label{sec:related_work_and_future_research_topics}

As a remedy to this potential exponential dependence on $ N $ in the reconstruction process, recent work has been done on high-dimensional function reconstruction from in sub-linear time with sub-linear storage necessities.
In contrast to $ \ell_1 $-minimization, the method uses a variant on the CoSaMP algorithm, where a fast search is made to identify the basis functions that contribute most to sparse approximations of the considered vectors.
By working with just a few of these functions at a time and then restricting attention to these most important indices, the need to enumerate the entire, exponential in $ N $ basis is avoided.
Thus, these sub-linear time methods would be more apt to handle the case of weakly anisotropic solutions which may arise when, for example, the coefficients of the Karhunen-Lo\`eve expansion plateau before decaying forcing a high degree truncation in order to maintain fidelity to the original problem.
Additionally, even when the solutions do exhibit anisotropic behavior, just as $ \ell_1 $-minimization is benefited by the introduction of corresponding weights, there may be opportunity to employ an analogous weighting to the support identification process in the sub-linear time algorithms.
Other considerations may involve improving suboptimalities in the algorithm as well as applications to general problems in learning high dimensional functions outside the context of parametric PDE.

Along similar lines, recent work in calculating Fourier transforms of high dimensional functions has made use of so-called rank-one lattices for deterministic high dimensional samples.
The efficient calculations of high-dimensional Fourier transforms also extend to calculating approximate Chebyshev coefficients, which can again allow for efficient approximations of solutions to the parametric PDE in a gPC expansion.
Improving these methods is still an area of active research especially for spare expansions, and by improving these techniques, it will be possible to provide good function approximations with efficient storage and time considerations.

% My current thought is to apply these methods in the context of uncertainty quantification or parameterized PDE, analogous to the sparse grid interpolation and $ \ell_1 $ minimization function approximation methods.
% This would likely involve deriving results on anisotropic regularity in the solution which are suited for identifying a support set to be used in the sub-linear time algorithm.
% Since the sub-linear algorithm is so efficient, this would hopefully widen the class of tractably solvable UQ problems with provable convergence results.
% Along these lines, in the context of function interpolation for other compressed sensing techniques, requirements involving $ W $, the $ L^\infty $ bound of the basis have been avoided by shifting to stronger requirements on the original function via forcing dependence on the $ L^\infty $ norm through weights.
% Thus, a similar shift could be investigated for the sub-linear method.
% Since the runtime of the method at least involves taking samples, it would be interesting to see how this would affect and possibly improve the already fast runtime.
% Numerical results in the context of UQ problems would be a strong priority as well.
% Other considerations may involve improving suboptimalities in the algorithm.
% Other considerations with regard to the parameterized PDE problem may involve considering classes beyond just affine parametric PDE and possibly exploring more intricate high-dimensional PDE which at the current time are intractable to solve.
% \todo[inline]{Like what? Do these even exist? Nonlinear? Something else?}

\end{document}
