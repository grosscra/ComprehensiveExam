\documentclass[11pt]{amsart}

\usepackage[T1]{fontenc}

\usepackage{beton} % Use Concrete font for text
\renewcommand{\bfdefault}{sbc} % Use alternate computer modern fonts for boldface
\usepackage[euler-digits,euler-hat-accent]{eulervm} % Use Euler font for math

\usepackage[margin=1.0in,footskip=0.25in,top=0.75in,left=.75in,right=.75in]{geometry}  % Margins

\usepackage{fancyhdr} % Header
\setlength{\headheight}{14.5pt}
\pagestyle{fancy}
\fancyfoot{}
\lhead{Probability} % Left header
\rhead{$ \Vert $ \thepage} % Right header

\usepackage{amsmath,amsfonts,amsthm,amssymb,mathtools} % Math stuff
\usepackage[italicdiff]{physics} % Derivatives
\usepackage{graphicx,caption,subcaption,tikz,pdfpages,tcolorbox,todonotes,enumitem,hyperref} % Graphics/extras


% === Math shortcuts ===


% Theorems
\newtheorem{thm}{Theorem}
\newtheorem{defn}{Definition}
\newtheorem{ex}{Example}
\newtheorem{rem}{Remark}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}
\newtheorem{cor}{Corollary}
\newtheorem{assum}{Assumption}

% Blackboard bold
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\E}{\mathbb{E}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\V}{\mathbb{V}}
\renewcommand{\L}{\mathbb{L}}
\renewcommand{\S}{\mathbb{S}}

% Real and complex parts
\renewcommand{\Im}{\mathrm{Im}}
\renewcommand{\Re}{\mathrm{Re}}

% Integral d
\renewcommand{\d}{\;d}

% Functions
\DeclareMathOperator{\cov}{cov}
\DeclareMathOperator{\linspan}{span}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\conv}{conv}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\nul}{null}
\DeclareMathOperator*{\minimize}{minimize}

% ======================

% Suppress warnings
\hbadness=99999

\begin{document}

\title{Every probability result you wanted to know to prove the weighted restricted isometry property for a gPC sampling matrix*}
\thanks{*but were too afraid to ask}
\maketitle

\begin{defn}
	\label{prop:SubGaussianProperties}
	A random variable $ X $ is \emph{sub-gaussian} if
		$ \P (|X| \geq t) \leq 2 \exp(-t^2 / K^2) $  for all $ t \geq 0$
	with \emph{sub-gaussian norm} $ \norm{ X }_{ \psi_2 } $ the smallest $ K $ for which this holds.
\end{defn}

\begin{thm}[Kintchine's Inequality]
	For $ X_1, \ldots, X_N $ independent sub-gaussian random variables with zero mean, let $ a = (a_i)_{ i = 1 }^N \in \R^N $. 
	Then for any $ p \in [2, \infty) $,
	$\norm{ \sum_{ i = 1 }^N X_i a_i }_{ L^p } \leq C K \sqrt{p} \norm{ a }_2$
	where $ K = \max_{ i \in [N] } \norm{ X_i }_{ \psi_2 } $, and $ C $ is an absolute constant.
\end{thm}

\begin{defn}
	A stochastic process $ (X_t)_{ t \in T } $ on a metric space $ (T, d) $ is \emph{sub-gaussian} if there exists a uniform constant $ G > 0 $ such that the increments satisfy
	$\norm{ X_s - X_t }_{ \psi_2 } \leq G d(s, t).$
\end{defn}

\begin{defn}
	Let $ ( \mathcal{X}, d ) $  be a metric space.
	Consider a subset $ K \subset \mathcal{X} $ and let $ \epsilon > 0 $.
	A subset $ \mathcal{N} \subseteq K $ is called an \emph{$ \epsilon $-net of $ K $} if every point in $ K $ is within a distance $ \epsilon $ of some point of $ \mathcal{N} $.
The smallest cardinality of an $ \varepsilon $-net of $ K $ is called the \emph{covering number} of $ K $ and is denoted $ \mathcal{N}(K, d, \varepsilon) $.
\end{defn}

\begin{thm}[Dudley's Inequality]
	\label{thm:DudleysInequality}
	A sub-gaussian process $ (X_t)_{ t \in T } $ satisfies
	\begin{equation*}
		\E \sup_{ t \in T } \abs{X_t - X_{t_0}} \leq C G \int_0^\infty \sqrt{\log \mathcal{N}(T, d, \epsilon)} \d \epsilon,
	\end{equation*}
	where $ G $ is the constant with respect to which $ X_t $ is sub-gaussian.
	For measurability purposes, we consider the \emph{lattice supremum} of the stochastic process,
	\begin{equation*}
		\E \sup_{ t \in T } |X_t - X_{ t_0 } | = \sup_{ \substack{ S \subseteq T \\ S \textrm{ finite}} } \E \sup_{ t \in S } |X_t - X_{ t_0 }|.
	\end{equation*}
\end{thm}

\begin{lem}[Symmetrization]
	\label{lem:Symmetrization}
	Let $ X_1, \ldots, X_N $ be independent random vectors in a normed space, and $ \varepsilon_1, \ldots \varepsilon_N $ an independent Rademacher sequence.
	Then
		$\E \norm{ \sum_{ i = 1 }^N (X_i - \E X_i) } \leq 2 \E \norm{ \sum_{ i = 1 }^N \varepsilon_i X_i }.$
\end{lem}

\begin{lem}[Maurey's Lemma]
	\label{lem:MaureysLemma}
	Let $ \mathcal{X} $ be a normed space, and consider a finite set $ \mathcal{U} \subset \mathcal{X} $ of $ N $ points.
	Assume that for every $ L \in \N $ and $ (u_1, \ldots, u_L) \in \mathcal{U}^L $, $ \E_\varepsilon \norm{ \sum_{ i = 1 }^L \varepsilon_i u_i } \leq A \sqrt{L} $ for a Rademacher sequence $ \varepsilon $ as above.
	Then for every $ t > 0 $
	\begin{equation*}
		\log \mathcal{N} (\conv( \mathcal{U} ), \norm{ \cdot }, t) \leq c (A / t)^2 \log N.
	\end{equation*}
\end{lem}

\begin{thm}[Bernstein's Inequality for Suprema of Empirical Processes]
	\label{thm:BernsteinForEmpiricalProcesses}
	Let $ \mathcal{F} $ be a countable set of functions $ F: \C^M \rightarrow \R $.
	Let $ X_1, \ldots X_K $ be independent random vectors in $ \C^M $ such that $ \E F(X_k) = 0 $, and $ F(X_k) \leq L $ almost surely for all $ k \in [K] $ and for all $ F \in \mathcal{F} $ for some constant $ L > 0 $.
	For the supremum of an empirical process
		$ Z = \sup_{ f \in \mathcal{F} } \sum_{ k = 1 }^K F(X_k), $ 
	which satisfies the bound on the variance of the summed terms $ \E[F(X_k)^2] \leq \sigma_k^2 $ for all $ F \in \mathcal{F} $ and $ k \in [K] $,
	we have for any $ t > 0 $,
	\begin{equation*}
		\P(Z \geq \E Z + t) \leq \exp( - \frac{t^2 / 2}{\sigma^2 + 2 L \E Z + t L / 3} ),
	\end{equation*}
	where $ \sigma^2 = \sum_{ k = 1 }^K \sigma_k^2 $ is the variance of the sum.
\end{thm}

\end{document}
